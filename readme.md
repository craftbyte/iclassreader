# iClassReader

This is intended as a user-friendly device to scan iClass fobs/cards, then display their card number (CN).

<img width="200" alt="Device" src="./device.jpeg" style="display: block; margin: 0 auto;"/>

## Hardware

I ended up basing it on the ST25R3911B dev kit from [waveshare](https://www.waveshare.com/st25r3911b-nfc-board.htm). To flash the device you'll need an stlink or other device to interface with the ST25R3911B's SWD interface.

## Software

- This is a [PlatformIO](https://github.com/RfidResearchGroup/proxmark3/) project.
- Code for iClass hashing, diversification, mac calculation come from [proxmark3](https://github.com/RfidResearchGroup/proxmark3/)
- Based on the [ST25R3911B-NFC-Demo](https://www.waveshare.com/w/upload/e/e3/ST25R3911B-NFC-Demo.zip) project on the [waveshare wiki](https://www.waveshare.com/wiki/ST25R3911B_NFC_Board)
- The useful starting point of the code is `src/iClassReader.c`; `src/main.c` initializes peripherals, handles system stuff, and starts an infinite loop.
- I added `_write` to usart.c to fix `printf` after porting the project to PlatformIO. I didn't investigate the underlying issue.
- `lib/rfal/rfal_iclass.*` was written by me, but patterned off of `lib/rfal_rfal_nfcv.*`.
- `lib/Browser/Browser.*` appears to be the main code written by waveshare and shows button interaction and screen drawing.
- `lib/mbedtls` is a copy of the current PlatformIO mbedtls library(mbedtls_ID2509) with a library.json that has been tweaked for the STM32

### Missing pieces

Since they were shared with me without the authority to re-share, and to avoid legal issues, the HID iClass key and iClass decryption key are omitted.
Place files in the root of the SD card:

- iclass_key.bin
- iclass_decryptionkey.bin

## In action

![video demonstration](demo.mp4)

## Future ideas

- Add Facility Code to UI, or button to swap between CN/FC
- Detect when card leaves field and set display back to message
- Set a button to 'clear' to clear number from screen
- Add timer (with shrinking line on display) that clears display
- Add on-boot logo
- Add power switch to exposed pins on right?
- Add battery
  - Use ST25R3911B capacitive touch sensor to keep device in lower power mode
- Add iClass CRC calculation and support reading other blocks
- Add dump of full card to SD Card

## Other References

- https://docs.platformio.org/en/latest/boards/ststm32/genericSTM32F103RB.html
