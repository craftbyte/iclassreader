/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _iClassReader_H
#define _iClassReader_H

/* Includes ------------------------------------------------------------------*/
#include "Fonts.h"
#include "GUI_Paint.h"
#include "Key.h"
#include "SD_Card_APP.h"
#include "SSD1306.h"
#include "demo.h"
#include "fatfs.h"
#include "rfal_iclass.h"
#include "rfal_nfcv.h"
#include "rfal_rf.h"
#include "stdbool.h"
#include "string.h"
#include "tim.h"

#include "cipher.h"
#include "ikeys.h"

#include <mbedtls/des.h>

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void iClassReader_setup();
void iClassReader_loop(void);

#endif
