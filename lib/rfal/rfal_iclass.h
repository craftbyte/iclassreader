
#ifndef RFAL_ICLASS_H
#define RFAL_ICLASS_H

/*
 ******************************************************************************
 * INCLUDES
 ******************************************************************************
 */
#include "platform.h"
#include "rfal_rf.h"
#include "st_errno.h"

#define RFAL_ICLASS_UID_LEN 8
#define RFAL_ICLASS_MAX_BLOCK_LEN 8

#define RFAL_ICLASS_TXRX_FLAGS (RFAL_TXRX_FLAGS_CRC_TX_MANUAL | RFAL_TXRX_FLAGS_AGC_ON | RFAL_TXRX_FLAGS_PAR_RX_REMV | RFAL_TXRX_FLAGS_CRC_RX_KEEP)

enum {
  RFAL_ICLASS_CMD_ACTALL = 0x0A,
  RFAL_ICLASS_CMD_IDENTIFY = 0x0C,
  RFAL_ICLASS_CMD_SELECT = 0x81,
  RFAL_ICLASS_CMD_READCHECK = 0x88,
  RFAL_ICLASS_CMD_CHECK = 0x05,
  RFAL_ICLASS_CMD_READ = 0x0C,
};

typedef struct {
  uint8_t CSN[RFAL_ICLASS_UID_LEN]; // Anti-collision CSN
  uint8_t crc[2];
} rfalIclassIdentifyRes;

typedef struct {
  uint8_t CSN[RFAL_ICLASS_UID_LEN]; // Real CSN
  uint8_t crc[2];
} rfalIclassSelectRes;

typedef struct {
  uint8_t CCNR[8];
} rfalIclassReadCheckRes;

typedef struct {
  uint8_t mac[4];
} rfalIclassCheckRes;

typedef struct {
  uint8_t data[RFAL_ICLASS_MAX_BLOCK_LEN];
  uint8_t crc[2];
} rfalIclassReadBlockRes;

ReturnCode rfalIclassPollerInitialize(void);
ReturnCode rfalIclassPollerCheckPresence(void);
ReturnCode rfalIclassPollerIdentify(rfalIclassIdentifyRes *idRes);
ReturnCode rfalIclassPollerSelect(uint8_t *csn, rfalIclassSelectRes *selRes);
ReturnCode rfalIclassPollerReadCheck(rfalIclassReadCheckRes *rcRes);
ReturnCode rfalIclassPollerCheck(uint8_t *mac, rfalIclassCheckRes *chkRes);
ReturnCode rfalIclassPollerReadBlockSeven( rfalIclassReadBlockRes *readRes);

#endif /* RFAL_ICLASS_H */
